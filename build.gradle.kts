// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {

    repositories {
        // firebase
        google()
        jcenter()
        maven("https://jitpack.io")
    }

    // Do not place application dependencies here; they belong in the
    // individual module build.gradle files
    dependencies {
        classpath("com.android.tools.build:gradle:7.0.0")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.31")
        // po editor for localized strings
        classpath("com.github.bq:poeditor-android-gradle-plugin:0.2.5")
        // firebase
        classpath("com.google.gms:google-services:4.3.4")
    }
}


allprojects {
    repositories {
        // firebase
        google()
        jcenter()
        maven("https://jitpack.io")
    }
}

tasks.register<Delete>("clean") {
    delete(rootProject.buildDir)
}
