plugins {
    kotlin("jvm")
    kotlin("kapt")
}

dependencies {
    // dependencies appearing in the api configurations will be transitively exposed
    // to consumers of the library; dependencies found in the implementation configuration
    // will not be exposed to consumers
    implementation(kotlin("stdlib-jdk8"))

    val retrofitVersion = "2.6.0"
    val daggerVersion = "2.34"
    val okHttpVersion = "3.11.0"

    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-moshi:$retrofitVersion")
    implementation("com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion")

    implementation("com.google.dagger:dagger:$daggerVersion")
    kapt("com.google.dagger:dagger-compiler:$daggerVersion")

    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
    implementation("com.squareup.okhttp3:okhttp:$okHttpVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:4.2.0")
    implementation("com.squareup.moshi:moshi-kotlin:1.8.0")
}

dependencies {
    testImplementation("io.mockk:mockk:1.9.3")
    testImplementation("org.assertj:assertj-core:3.12.2")
    testImplementation("junit:junit:4.13.1")
    testImplementation("org.mockito:mockito-core:3.0.0")
}
