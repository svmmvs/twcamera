package su.network.model

import com.squareup.moshi.Json

/**
 * JSON format:
 *     {"id":"0",
 *     "author":"Alex",
 *     "width":5000,
 *     "height":4000,
 *     "url":"https://url",
 *     "download_url":"https://url"}
 */
data class AwesomeImage(

    val id: String,
    val author: String,
    val width: String,
    val height: String,
    @Json(name = "url") val originUrl: String,
    @Json(name = "download_url") val downloadUrl: String
)
