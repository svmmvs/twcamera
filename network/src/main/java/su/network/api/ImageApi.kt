package su.network.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import su.network.model.AwesomeImage

interface ImageApi {
    /**
     * Get random images from site
     *
     * [
     * {"id":"0","author":"Alejandro","width":5616,"height":3744,"url":"https://unsplash.com/photos/yC-Yzbqy7PY","download_url":"https://picsum.photos/id/0/5616/3744"},
     * {"id":"1","author":"Alejandro","width":5616,"height":3744,"url":"https://unsplash.com/photos/LNRyGwIJr5c","download_url":"https://picsum.photos/id/1/5616/3744"},
     * {"id":"9","author":"Matthew","width":5760,"height":3840,"url":"https://unsplash.com/photos/tBtuxtLvAZs","download_url":"https://picsum.photos/id/1005/5760/3840"}
     * ]
     *
     */
    @GET("/v2/list")
    fun getImages(@Query("limit") numberOfImages: Int): Single<Response<List<AwesomeImage>>>
}
