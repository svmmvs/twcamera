package su.network.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

const val RETROFIT_PIC_SUM = "retrofit_pic_sum"
const val RETROFIT_BACKEND = "retrofit_backend"
private const val PIC_SUM_URL = "https://picsum.photos"
private const val CONNECT_TIMEOUT_SECONDS = 10

@Module
open class NetworkModule(private val backendUrl: String = "") {
    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }

    @Provides
    @Singleton
    open fun provideHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
        val builder = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
            .addInterceptor(logging)
        return builder.build()
    }

    @Provides
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit.Builder {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
    }

    @Provides
    @Singleton
    @Named(RETROFIT_BACKEND)
    fun provideRetrofitBackend(builder: Retrofit.Builder): Retrofit {
        return builder.baseUrl(backendUrl).build()
    }

    @Provides
    @Singleton
    @Named(RETROFIT_PIC_SUM)
    fun provideRetrofitPic(builder: Retrofit.Builder): Retrofit {
        return builder.baseUrl(PIC_SUM_URL).build()
    }
}
