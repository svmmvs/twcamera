plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    // firebase
    id("com.google.gms.google-services")
}

android {
    compileSdk = 31

    defaultConfig {
        applicationId = "su.twcamera"
        minSdk = 21
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"

        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    flavorDimensions.add("distribution")

    productFlavors {
        create("ci") {
            dimension = "distribution"
            applicationIdSuffix = ".ci"
        }
        create("dev") {
            dimension = "distribution"
        }
        create("play") {
            dimension = "distribution"
        }
    }

    // enable jetpack compose for app module
    buildFeatures {
        compose = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = "11"
    }

    composeOptions {
        kotlinCompilerExtensionVersion = "1.0.5"
    }

    sourceSets {
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
    }

    packagingOptions {
        resources.excludes.add("META-INF/*")
    }
}

dependencies {
    implementation(
        fileTree(
            mapOf(
                "dir" to "libs",
                "include" to listOf("*.aar", "*.jar")
            )
        )
    )
    implementation(kotlin("stdlib-jdk8"))

    val retrofitVersion = "2.6.0"
    val daggerVersion = "2.34"
    val okHttpVersion = "3.11.0"

    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-moshi:$retrofitVersion")
    implementation("com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion")

    implementation("com.google.dagger:dagger:$daggerVersion")
    kapt("com.google.dagger:dagger-compiler:$daggerVersion")

    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
    implementation("com.squareup.okhttp3:okhttp:$okHttpVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:4.2.0")
    implementation("com.squareup.moshi:moshi-kotlin:1.8.0")

    // jetpack compose
    implementation("androidx.activity:activity-compose:1.4.0")
    implementation("androidx.compose.material:material:1.0.5")
    implementation("androidx.compose.animation:animation:1.0.5")
    implementation("androidx.compose.ui:ui-tooling:1.0.5")
    implementation("com.google.accompanist:accompanist-appcompat-theme:0.16.0")
    // Observe state in Composable
    implementation("androidx.compose.runtime:runtime-livedata:1.0.5")
    // Image loading in Composable
    implementation("io.coil-kt:coil-compose:1.4.0")

    // android x
    implementation("androidx.appcompat:appcompat:1.3.1")
    implementation("androidx.core:core-ktx:1.3.2")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("androidx.recyclerview:recyclerview:1.1.0")
    implementation("androidx.vectordrawable:vectordrawable:1.1.0")
    implementation("androidx.annotation:annotation:1.1.0")
    implementation("com.google.android.material:material:1.2.1")
    implementation("androidx.fragment:fragment-ktx:1.4.0")

    val lifecycleVersion = "2.4.0"
    // Jetpack Compose
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:$lifecycleVersion")
    // ViewModel
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion")
    // LiveData
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion")
    // Lifecycles only (without ViewModel or LiveData)
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion")
    // Saved state module for ViewModel
    implementation("androidx.lifecycle:lifecycle-viewmodel-savedstate:$lifecycleVersion")
    // Annotation processor
    kapt("androidx.lifecycle:lifecycle-compiler:$lifecycleVersion")

    // camera
    val cameraXVersion = "1.0.0-alpha06"
    implementation("androidx.camera:camera-core:$cameraXVersion")
    implementation("androidx.camera:camera-camera2:$cameraXVersion")

    // open cv
    implementation("com.quickbirdstudios:opencv:3.4.1")

    // modules
    implementation(project(":util"))
    implementation(project(":network"))
}

dependencies {
    val junitLibrary = "junit:junit:4.13.1"
    val assertJLibrary = "org.assertj:assertj-core:3.12.2"
    val coreTestLibrary = "androidx.arch.core:core-testing:2.1.0"
    val mockitoVersion = "3.0.0"

    testImplementation(junitLibrary)
    testImplementation(assertJLibrary)
    testImplementation(coreTestLibrary)
    testImplementation("org.mockito:mockito-core:$mockitoVersion")
    testImplementation("io.mockk:mockk:1.9.3")

    androidTestImplementation(junitLibrary)
    androidTestImplementation(assertJLibrary)
    androidTestImplementation(coreTestLibrary)
    // https://javadoc.io/static/org.mockito/mockito-core/4.2.0/org/mockito/Mockito.html#mockito-android
    // native android support by mockito;
    // 'com.linkedin.dexmaker:dexmaker-mockito:2.X.X' (includes mockito) not necessary
    androidTestImplementation("org.mockito:mockito-android:$mockitoVersion")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:1.0.5")
}

/* for JUnit5
- add dependency:
testImplementation 'org.junit.jupiter:junit-jupiter:5.8.0-M1'
- add to gradle:
tasks.withType<Test> {
    useJUnitPlatform()
}
 */
