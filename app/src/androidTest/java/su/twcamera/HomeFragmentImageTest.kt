package su.twcamera

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.compose.material.MaterialTheme
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import retrofit2.Response
import su.network.model.AwesomeImage
import su.twcamera.home.HomeFragment
import su.twcamera.home.HomeViewModel

@RunWith(AndroidJUnit4::class)
class HomeFragmentImageTest {

    @Rule
    @JvmField
    val composeTestRule = createAndroidComposeRule<MasterActivity>()

    @Rule
    @JvmField
    val instantRule = InstantTaskExecutorRule()

    private lateinit var mockRepository: ImageRepository

    private lateinit var mockViewModel: HomeViewModel

    @Before
    fun setUp() {
        mockRepository = mock(ImageRepository::class.java)

        given(mockRepository.requestAwesomeImages()).willReturn(
            Single.just(
                Response.success(
                    listOf(
                        AwesomeImage(
                            id = "1",
                            author = "Charles",
                            width = "1",
                            height = "1",
                            originUrl = "origin",
                            downloadUrl = "https://downloadthis.com"
                        ),
                        AwesomeImage(
                            id = "2",
                            author = "Joanna",
                            width = "1",
                            height = "1",
                            originUrl = "origin",
                            downloadUrl = "https://downloadthis.com"
                        ),
                        AwesomeImage(
                            id = "3",
                            author = "Leonardo",
                            width = "1",
                            height = "1",
                            originUrl = "origin",
                            downloadUrl = "https://downloadthis.com"
                        )
                    )
                )
            )
        )
    }

    @Test
    fun testHomeImage() {
        mockViewModel = HomeViewModel(mockRepository)
        composeTestRule.setContent {
            MaterialTheme {
                HomeFragment(mockViewModel)
            }
        }
        composeTestRule.onNodeWithContentDescription("Android Image").assertIsDisplayed()
    }

    @Test
    fun testSelectItemState() {
        mockViewModel = HomeViewModel(mockRepository)
        composeTestRule.setContent {
            MaterialTheme {
                HomeFragment(mockViewModel)
            }
        }

        composeTestRule.onNodeWithContentDescription("Item 2").apply {
            assertIsDisplayed()
            performClick()
        }
        assertThat(mockViewModel.getSelectedIndex().value).isEqualTo(1)
    }
}
