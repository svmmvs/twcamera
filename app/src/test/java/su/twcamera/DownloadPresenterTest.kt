package su.twcamera

import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.*
import org.mockito.InjectMocks
import org.mockito.Mockito.mock
import retrofit2.Response
import su.network.model.AwesomeImage
import su.twcamera.download.DownloadPresenter
import su.twcamera.download.DownloadView
import su.twcamera.util.BasePresenterTest

class DownloadPresenterTest : BasePresenterTest() {

    @InjectMocks
    private lateinit var presenter: DownloadPresenter

    private lateinit var mockRepository: ImageRepository

    private lateinit var viewMock: DownloadView

    private val fakeImage = AwesomeImage(
        id = "8",
        author = "Lee",
        width = "100",
        height = "100",
        originUrl = "origin.com",
        downloadUrl = "download.here"
    )

    @Before
    override fun setUp() {
        viewMock = mock(DownloadView::class.java)
        mockRepository = mock(ImageRepository::class.java)
        presenter = DownloadPresenter(mockRepository)
        presenter.bind(viewMock)
    }

    override fun tearDown() {
        presenter.unbind()
    }

    @Test
    fun onSuccessfullyLoading() {
        given(mockRepository.requestAwesomeImages()).willReturn(
            Single.just(
                Response.success(
                    listOf(
                        fakeImage
                    )
                )
            )
        )
        // given the presenter downloading images
        presenter.downloadAwesomeImages()
        // then the view should show the images list and update the list
        then(viewMock).should(atLeastOnce()).showLoading(true)
        then(viewMock).should().showImageList()
        then(viewMock).should().updateImagesList()
        then(viewMock).should(atLeastOnce()).showLoading(false)
    }
}
