package su.twcamera.util

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers

object RxTestUtil {
    /**
     * Replaces the [Schedulers.io] and [AndroidSchedulers.mainThread] schedulers
     * with versions that execute synchronously for test purposes.
     */
    fun makeSchedulersImmediate() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }

        // Be synchronous, and also ensure that we don't need to touch Android Looper
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    /**
     * Removes the scheduler hooks set in [makeSchedulersImmediate].
     */
    fun removeSchedulerHooks() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}
