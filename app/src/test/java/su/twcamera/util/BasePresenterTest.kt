package su.twcamera.util

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass

abstract class BasePresenterTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun beforeClass() {
            RxTestUtil.makeSchedulersImmediate()
        }

        @AfterClass
        @JvmStatic
        fun afterClass() {
            RxTestUtil.removeSchedulerHooks()
        }
    }

    @Before
    abstract fun setUp()

    @After
    abstract fun tearDown()
}
