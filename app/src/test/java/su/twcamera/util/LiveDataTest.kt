package su.twcamera.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import su.twcamera.mvvm.Event
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Test live data objects; observe live data and receive last emitted item
 */
object LiveDataTest {

    @Throws(InterruptedException::class)
    fun <T> getValue(liveData: LiveData<T>): T? {
        var data: T? = null
        val latch = CountDownLatch(1)

        val observer = object : Observer<T> {
            override fun onChanged(t: T?) {
                data = t
                latch.countDown()
                liveData.removeObserver(this)
            }
        }
        liveData.observeForever(observer)
        latch.await(1, TimeUnit.SECONDS)
        return data
    }

    @Throws(InterruptedException::class)
    fun <T> getEventValue(liveData: LiveData<Event<T>>): T? {
        return getValue(liveData)?.consume()
    }
}
