package su.twcamera

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Single
import io.reactivex.subjects.SingleSubject
import okhttp3.ResponseBody.Companion.toResponseBody
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import retrofit2.Response
import su.network.model.AwesomeImage
import su.twcamera.home.HomeUiModel
import su.twcamera.home.HomeViewModel
import su.twcamera.mvvm.Resource
import su.twcamera.util.LiveDataTest
import su.twcamera.util.RxTestUtil

class HomeViewModelTest {

    @Rule
    @JvmField
    val instantRule = InstantTaskExecutorRule()

    private lateinit var mockRepository: ImageRepository

    private lateinit var mockViewModel: HomeViewModel

    private val mockResponseList = listOf(
        AwesomeImage(
            id = "1",
            author = "Charles",
            width = "1",
            height = "1",
            originUrl = "origin",
            downloadUrl = "https://downloadthis.com"
        ),
        AwesomeImage(
            id = "2",
            author = "Joanna",
            width = "1",
            height = "1",
            originUrl = "origin",
            downloadUrl = "https://downloadthis.com"
        ),
        AwesomeImage(
            id = "3",
            author = "Leonardo",
            width = "1",
            height = "1",
            originUrl = "origin",
            downloadUrl = "https://downloadthis.com"
        )
    )

    private val mockViewModelList =
        mockResponseList.map { HomeUiModel(author = it.author, imageLink = it.downloadUrl) }

    @Before
    fun setUp() {
        RxTestUtil.makeSchedulersImmediate()
        mockRepository = mock(ImageRepository::class.java)

        given(mockRepository.requestAwesomeImages()).willReturn(
            Single.just(Response.success(emptyList()))
        )
    }

    @After
    fun tearDown() {
        RxTestUtil.removeSchedulerHooks()
    }

    @Test
    fun receiveListsSuccessfully() {
        given(mockRepository.requestAwesomeImages()).willReturn(
            Single.just(Response.success(mockResponseList))
        )
        mockViewModel = HomeViewModel(mockRepository)
        val result = LiveDataTest.getValue(mockViewModel.getAwesomeImages())
        assertThat(result).isEqualTo(Resource.Success(mockViewModelList))
    }

    @Test
    fun receiveServerError() {
        given(mockRepository.requestAwesomeImages()).willReturn(
            Single.just(
                Response.error(
                    403,
                    "".toResponseBody(null)
                )
            )
        )
        mockViewModel = HomeViewModel(mockRepository)
        val result = LiveDataTest.getValue(mockViewModel.getAwesomeImages())
        assertThat(result).isInstanceOf(Resource.Error::class.java)
        assertThat((result as Resource.Error).errorCode).isEqualTo(403)
    }

    @Test
    fun loadingAndSuccess() {
        val listEmitter = SingleSubject.create<Response<List<AwesomeImage>>>()
        given(mockRepository.requestAwesomeImages()).willReturn(listEmitter)
        mockViewModel = HomeViewModel(mockRepository)
        // Single not finished, loading state
        assertThat(LiveDataTest.getValue(mockViewModel.getAwesomeImages())).isEqualTo(
            Resource.Loading(
                null
            )
        )
        listEmitter.onSuccess(Response.success(mockResponseList))
        // Single is terminated, success state
        assertThat(LiveDataTest.getValue(mockViewModel.getAwesomeImages())).isEqualTo(
            Resource.Success(
                mockViewModelList
            )
        )
    }
}
