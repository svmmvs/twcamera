package su.twcamera.camerapreview

class CameraPreviewPresenter {

    private var cameraView: CameraPreviewView? = null

    fun bind(cameraView: CameraPreviewView) {
        this.cameraView = cameraView
    }

    fun unbind() {
        cameraView = null
    }

    fun filterClick() {
        cameraView?.setFilter()
    }

    fun unfilterClick() {
        cameraView?.removeFilter()
    }
}
