package su.twcamera.camerapreview

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Matrix
import android.os.Bundle
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.view.*
import android.widget.Button
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc
import su.twcamera.R
import su.util.views.SnackBarUtil
import java.util.concurrent.Executors


private const val PERMISSION_REQUEST_CODE = 888
private val PERMISSIONS_REQUIRED = arrayOf(Manifest.permission.CAMERA)

class CameraPreviewFragment : Fragment(), CameraPreviewView, LifecycleOwner {

    private lateinit var cameraPresenter: CameraPreviewPresenter

    private lateinit var root: View

    private lateinit var filterButton: Button

    private lateinit var unFilterButton: Button

    private lateinit var viewFinder: TextureView

    private var filterOn: Boolean = false

    private lateinit var loaderCallback: BaseLoaderCallback

    private val executor = Executors.newSingleThreadExecutor()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cameraPresenter = CameraPreviewPresenter()
        cameraPresenter.bind(this)

        loaderCallback = object : BaseLoaderCallback(context) {
            override fun onManagerConnected(status: Int) {
                super.onManagerConnected(status)
                when (status) {
                    SUCCESS -> Log.d("OpenCV", "OpenCV success")
                    else -> Log.d("OpenCV", "OpenCV fail")
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_camera_preview, container, false)

        filterButton = root.findViewById(R.id.camera_filter_button)
        filterButton.setOnClickListener { cameraPresenter.filterClick() }
        unFilterButton = root.findViewById(R.id.camera_unfilter_button)
        unFilterButton.setOnClickListener { cameraPresenter.unfilterClick() }

        viewFinder = root.findViewById(R.id.camera_preview_square)
        if (allPermissionsGranted()) {
            viewFinder.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(
                activity!!, PERMISSIONS_REQUIRED, PERMISSION_REQUEST_CODE
            )
        }

        // texture view changes, change layout
        viewFinder.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {
            Log.d(
                "OpenCV",
                "Internal OpenCV library not found. Using OpenCV Manager for initialization"
            )
            OpenCVLoader.initAsync(
                OpenCVLoader.OPENCV_VERSION_3_0_0,
                context,
                loaderCallback
            )
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!")
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    override fun onDestroy() {
        cameraPresenter.unbind()
        super.onDestroy()
    }

    override fun setFilter() {
        filterOn = true
    }

    override fun removeFilter() {
        filterOn = false
    }

    private fun startCamera() {
        // Create configuration object for the viewfinder use case
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetResolution(Size(480, 480))
        }.build()

        // Build the viewfinder use case
        val preview = Preview(previewConfig)

        // Every time the viewfinder is updated, recompute layout
        preview.setOnPreviewOutputUpdateListener {
            val parent = viewFinder.parent as ViewGroup
            parent.removeView(viewFinder)
            parent.addView(viewFinder, 0)
            viewFinder.setSurfaceTexture(it.surfaceTexture)
            updateTransform()
        }

        val imageAnalysis = setImageAnalysis()

        CameraX.bindToLifecycle(this, preview, imageAnalysis)
    }

    private fun setImageAnalysis(): ImageAnalysis {
        val analyzerThread = HandlerThread("OpenCVAnalysis")
        analyzerThread.start()

        val imageAnalysisConfig = ImageAnalysisConfig.Builder()
            .setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
//            .setCallbackHandler(Handler(analyzerThread.looper))
            .setImageQueueDepth(1)
            .build()

        val imageAnalysis = ImageAnalysis(imageAnalysisConfig)

        imageAnalysis.setAnalyzer(executor,
            ImageAnalysis.Analyzer { _, _ ->
                // Analyzing live camera

                val bitmap = viewFinder.bitmap ?: return@Analyzer
                val mat = Mat()
                Utils.bitmapToMat(bitmap, mat)

                Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2GRAY)
                Utils.matToBitmap(mat, bitmap)
            })

        return imageAnalysis

    }

    private fun updateTransform() {
        val matrix = Matrix()
        // Compute the center of the view finder
        val centerX = viewFinder.width / 2f
        val centerY = viewFinder.height / 2f

        // Correct preview output to account for display rotation
        val rotationDegrees = when (viewFinder.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)
        viewFinder.setTransform(matrix)
    }

    /**
     * Result from permission request dialog. If granted, start camera, otherwise display snackBar
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (allPermissionsGranted()) {
                viewFinder.post { startCamera() }
            } else {
                SnackBarUtil.showHint("Permissions not granted!", root)
                // make interface and let activity finish fragment
//                activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = PERMISSIONS_REQUIRED.all {
        ContextCompat.checkSelfPermission(context!!, it) == PackageManager.PERMISSION_GRANTED
    }
}
