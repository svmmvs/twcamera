package su.twcamera.camerapreview

interface CameraPreviewView {
    fun setFilter()
    fun removeFilter()
}
