package su.twcamera.download

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import su.network.api.ImageApi
import su.twcamera.ImageRepository
import su.twcamera.MasterApp
import su.twcamera.R
import su.util.views.SnackBarUtil
import javax.inject.Inject

class DownloadFragment : Fragment(), DownloadView, RecyclerItemClickListener {

    @Inject
    lateinit var imageRepository: ImageRepository

    private lateinit var downloadPresenter: DownloadPresenter

    private lateinit var root: View

    private lateinit var imagesListView: RecyclerView

    private lateinit var progressBar: ProgressBar

    private lateinit var downloadButton: Button

    init {
        MasterApp.networkComponent().inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        downloadPresenter = DownloadPresenter(imageRepository)
        downloadPresenter.bind(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        root = inflater.inflate(R.layout.fragment_download, container, false)

        imagesListView = root.findViewById(R.id.images_list_view) as RecyclerView
        imagesListView.setHasFixedSize(true)
        imagesListView.layoutManager = LinearLayoutManager(context)

        progressBar = root.findViewById(R.id.download_progressbar)
        downloadButton = root.findViewById(R.id.get_images_button)
        downloadButton.setOnClickListener { downloadPresenter.downloadAwesomeImages() }

        return root
    }

    override fun onDestroy() {
        downloadPresenter.unbind()
        super.onDestroy()
    }

    override fun updateImagesList() {
        val imageAdapter = AwesomeImageAdapter(downloadPresenter)
        imageAdapter.setClickListener(this)
        imagesListView.adapter = imageAdapter
    }

    override fun openIntent(author: String, downloadUrl: String) {
//        val intent = Intent(context, DownloadFragment::class.java).apply {
//            // Parcelable
//            putExtra("author", author)
//            putExtra("link", downloadUrl)
//        }
//        startActivity(intent)
        Toast.makeText(context, "Open Image Url with Picasso etc.", Toast.LENGTH_LONG).apply {
            setGravity(Gravity.CENTER, 0, 0)
        }.show()
    }

    override fun onItemClick(position: Int) {
        downloadPresenter.prepareIntent(position)
    }

    override fun showSnackBar(message: String) {
        SnackBarUtil.showHint(message, root)
    }

    override fun showLoading(show: Boolean) {
        if (show) progressBar.visibility = VISIBLE else progressBar.visibility = GONE
    }

    override fun showImageList() {
        imagesListView.visibility = VISIBLE
    }
}
