package su.twcamera.download

import android.annotation.SuppressLint
import io.reactivex.android.schedulers.AndroidSchedulers
import su.network.model.AwesomeImage
import su.twcamera.ImageRepository
import su.util.logs.UtilLogcat

class DownloadPresenter(private val imageRepository: ImageRepository) {

    private var downloadView: DownloadView? = null

    private var data: List<AwesomeImage>? = null

    fun bind(downloadView: DownloadView) {
        this.downloadView = downloadView
    }

    fun unbind() {
        downloadView = null
    }

    @SuppressLint("CheckResult")
    fun downloadAwesomeImages() {
        imageRepository.requestAwesomeImages()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { downloadView?.showLoading(true) }
            .subscribe({ response ->
                if (response.isSuccessful) {
                    data = response.body()
                    downloadView?.apply {
                        showImageList()
                        updateImagesList()
                    }
                } else {
                    showSnackBar("Download failed!")
                }
                downloadView?.showLoading(false)
            }, {
                UtilLogcat.printError(it)
                showSnackBar("Download failed!")
                downloadView?.showLoading(false)
            })
    }

    fun prepareIntent(position: Int) {
        val clickItem = data!![position]
        val title = clickItem.author
        val code = clickItem.downloadUrl
        downloadView?.openIntent(title, code) ?: showSnackBar("No view!")
    }

    fun bindRowAtPosition(view: AwesomeImagesRowView, position: Int) {
        val item = data!![position]
        view.setRowText(item.author, item.downloadUrl)
    }

    fun rowsCount(): Int {
        return if (data == null) 0 else data!!.size
    }

    fun showSnackBar(message: String) {
        downloadView?.showSnackBar(message)
    }
}
