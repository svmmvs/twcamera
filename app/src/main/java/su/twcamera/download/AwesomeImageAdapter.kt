package su.twcamera.download

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import su.twcamera.R

class AwesomeImageAdapter(private val presenter: DownloadPresenter) :
    RecyclerView.Adapter<AwesomeImageAdapter.ImageHolder>() {

    private var clickListener: RecyclerItemClickListener? = null

    inner class ImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener, AwesomeImagesRowView {

        private val itemTitle: TextView = itemView.findViewById(R.id.image_author_title)

        private val itemSubtitle: TextView = itemView.findViewById(R.id.image_link_subtitle)

        init {
            itemView.setOnClickListener(this)
        }

        override fun setRowText(author: String, link: String) {
            itemTitle.text = author
            itemSubtitle.text = link
        }

        override fun onClick(v: View) {
            clickListener?.onItemClick(adapterPosition)
                ?: presenter.showSnackBar("No ClickListener set!")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val inflated = LayoutInflater.from(parent.context)
            .inflate(R.layout.image_item_list, parent, false)
        return ImageHolder(inflated)
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        presenter.bindRowAtPosition(holder, position)
//        val item = data!![position]
//        holder.item.setRowText(item.author, item.downloadUrl)
    }

    override fun getItemCount() = presenter.rowsCount()
    //return images == null ? 0 : images.getImages().size();

    fun setClickListener(clickListener: RecyclerItemClickListener) {
        this.clickListener = clickListener
    }
}

interface AwesomeImagesRowView {
    fun setRowText(author: String, link: String)
}

interface RecyclerItemClickListener {
    fun onItemClick(position: Int)
}
