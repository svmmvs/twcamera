package su.twcamera.download

interface DownloadView {
    fun updateImagesList()
    fun openIntent(author: String, downloadUrl: String)
    fun showSnackBar(message: String)
    fun showLoading(show: Boolean)
    fun showImageList()
}
