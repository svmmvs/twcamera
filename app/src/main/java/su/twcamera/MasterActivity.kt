package su.twcamera

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import su.twcamera.camerapreview.CameraPreviewFragment
import su.twcamera.download.DownloadFragment
import su.twcamera.home.HomeFragment
import su.util.logs.FIRE_BASE_TAG
import su.util.logs.UtilLogcat.printMessage

class MasterActivity : AppCompatActivity(), OnNavigationItemSelectedListener {

    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        // Welcome Screen
        bottomNavigationView.selectedItemId = R.id.action_start

//        Google services required
//        GoogleApiAvailability.makeGooglePlayServicesAvailable()
        logFirebaseToken()
        getFirebaseTopic()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // todo move to presenter
        return when (item.itemId) {
            R.id.action_start -> {
                setActionBarTitle(R.string.bottom_navigation_menu_one)
                transferFragment(HomeFragment())
                true
            }
            R.id.action_camera -> {
                setActionBarTitle(R.string.bottom_navigation_menu_two)
                transferFragment(CameraPreviewFragment())
                true
            }
            R.id.action_download -> {
                setActionBarTitle(R.string.bottom_navigation_menu_three)
                transferFragment(DownloadFragment())
                true
            }
            else -> true
        }
    }

    private fun setActionBarTitle(@StringRes titleId: Int) {
        supportActionBar?.title = getString(titleId)
    }

    private fun transferFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_content, fragment)
            .setReorderingAllowed(true)
            .commitAllowingStateLoss()
    }

    private fun logFirebaseToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    printMessage(FIRE_BASE_TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token
                printMessage(FIRE_BASE_TAG, token!!)
            })
    }

    private fun getFirebaseTopic() {
        // dirty topic workaround for tokens
        FirebaseMessaging.getInstance().subscribeToTopic("admin-77777")
            .addOnCompleteListener { task ->
                var msg = getString(R.string.firebase_personal_topic_subscribe)
                if (!task.isSuccessful) {
                    msg = getString(R.string.firebase_personal_topic_subscribe_fail)
                }
                printMessage(FIRE_BASE_TAG, msg)
            }
    }

//    private fun showBadToast() {
//        Toast.makeText(this, "Not there yet!", Toast.LENGTH_LONG).apply {
//            setGravity(Gravity.CENTER, 0, 0)
//        }.show()
//    }
}
