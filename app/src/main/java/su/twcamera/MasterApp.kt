package su.twcamera

import android.app.Application
import su.network.di.NetworkModule
import su.twcamera.di.*

open class MasterApp : Application() {
    companion object {
        lateinit var networkComponent: NetworkComponent

        fun networkComponent(): NetworkComponent {
            return networkComponent
        }
    }

    override fun onCreate() {
        super.onCreate()
        setupNetworkComponent()
    }

    private fun setupNetworkComponent() {
        networkComponent = DaggerNetworkComponent.builder()
            .networkModule(NetworkModule())
            .apiModule(ApiModule())
            .repositoryModule(RepositoryModule())
            .build()
    }
}
