package su.twcamera.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import su.twcamera.ImageRepository
import su.twcamera.mvvm.Resource
import su.util.logs.UtilLogcat

class HomeViewModel(private val imageRepository: ImageRepository) : ViewModel() {

    private val homeUiModel = MutableLiveData<Resource<List<HomeUiModel>>>()

    private var homeCurrentIndex = MutableLiveData(-1)

    private lateinit var imageDisposable: Disposable

    init {
        loadImages()
    }

    override fun onCleared() {
        imageDisposable.dispose()
    }

    fun onErrorRetry() {
        loadImages()
    }

    fun getAwesomeImages(): LiveData<Resource<List<HomeUiModel>>> {
        return homeUiModel
    }

    fun getSelectedIndex(): LiveData<Int> {
        return homeCurrentIndex
    }

    fun selectImageIndex(currentIndex: Int) {
        homeCurrentIndex.value = currentIndex
    }

    private fun loadImages() {
        // async operation to fetch images
        homeUiModel.value = Resource.Loading(null)
        imageDisposable = imageRepository.requestAwesomeImages()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                if (response.isSuccessful) {
                    homeUiModel.value = Resource.Success(
                        response.body()!!.map {
                            HomeUiModel(
                                author = it.author,
                                imageLink = it.downloadUrl
                            )
                        }
                    )
                } else {
                    homeUiModel.value =
                        Resource.Error(IllegalStateException("Server Error"), response.code())
                }
            }, {
                UtilLogcat.printError(it)
                homeUiModel.value = Resource.Error(it)
            })
    }
}
