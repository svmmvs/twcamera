package su.twcamera.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import su.twcamera.ImageRepository
import su.twcamera.MasterApp
import javax.inject.Inject

class HomeViewModelFactory : ViewModelProvider.Factory {

    @Inject
    lateinit var imageRepository: ImageRepository

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        MasterApp.networkComponent().inject(this)
        return HomeViewModel(imageRepository) as T
    }
}
