package su.twcamera.home


data class HomeUiModel(

    val author: String,

    val imageLink: String
)
