package su.twcamera.home

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import coil.compose.rememberImagePainter
import coil.size.Scale
import coil.transform.CircleCropTransformation
import su.twcamera.R
import su.twcamera.mvvm.Resource

class HomeFragment : Fragment() {

    private val model: HomeViewModel by viewModels(factoryProducer = { HomeViewModelFactory() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed
            )
            setContent {
                MaterialTheme {
                    HomeFragment(model)
                }
            }
        }
    }
}

@Composable
fun HomeFragment(model: HomeViewModel) {
    HomeList(
        listData = model.getAwesomeImages(),
        selectedIndex = model.getSelectedIndex(),
        onSelectIndex = model::selectImageIndex,
        onRetry = model::onErrorRetry
    )
}

@Composable
fun HomeList(
    listData: LiveData<Resource<List<HomeUiModel>>>,
    selectedIndex: LiveData<Int>,
    onSelectIndex: (Int) -> Unit,
    onRetry: () -> Unit
) {
    val listItems by listData.observeAsState()
    val currentSelect by selectedIndex.observeAsState(-1)
    val bigPadding = dimensionResource(id = R.dimen.padding_big_material)
    val itemHeight = dimensionResource(id = R.dimen.list_item_height)
    val iconSize = dimensionResource(id = R.dimen.icon_normal_size)
    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_android_green_art),
            contentDescription = "Android Image",
            modifier = Modifier
                .size(itemHeight)
                .padding(top = bigPadding)
        )

        when (val response: Resource<List<HomeUiModel>>? = listItems) {
            is Resource.Success -> {
                ListViewComponent(
                    successResponse = response,
                    currentSelect = currentSelect,
                    onSelectIndex = onSelectIndex
                )
            }
            is Resource.Loading -> {
                LoadingComponent(progressBarSize = iconSize)
            }
            is Resource.Error -> {
                ErrorComponent(
                    paddings = bigPadding,
                    errorResponse = response,
                    onRetry = onRetry
                )
            }
        }
    }
}

@Composable
fun ListViewComponent(
    successResponse: Resource.Success<List<HomeUiModel>>,
    currentSelect: Int,
    onSelectIndex: (Int) -> Unit
) {
    // TODO empty state
    LazyColumn {
        itemsIndexed(items = successResponse.data) { index, item ->
            ImageViewHolder(
                homeModel = item,
                index = index,
                currentIndex = currentSelect,
                onClick = onSelectIndex
            )
        }
    }
}

@Composable
fun LoadingComponent(progressBarSize: Dp) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator(
            color = Color.Red,
            modifier = Modifier.size(progressBarSize)
        )
    }
}

@Composable
fun ErrorComponent(
    paddings: Dp,
    errorResponse: Resource.Error,
    onRetry: () -> Unit
) {
    val serverError = stringResource(id = R.string.home_server_error)
    val retryText = stringResource(id = R.string.home_error_retry)
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddings),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "$serverError (${errorResponse.errorCode})",
            textAlign = TextAlign.Center,
            color = Color.Red,
            maxLines = 2,
            fontSize = 18.sp
        )
        Button(
            onClick = onRetry,
            modifier = Modifier.padding(top = paddings),
        ) {
            Text(text = retryText)
        }
    }
}

@Composable
fun ImageViewHolder(homeModel: HomeUiModel, index: Int, currentIndex: Int, onClick: (Int) -> Unit) {
    val smallPadding = dimensionResource(id = R.dimen.padding_small_material)
    val miniPadding = dimensionResource(id = R.dimen.padding_mini_material)
    val pickedColor = if (index == currentIndex) {
        MaterialTheme.colors.secondary
    } else {
        MaterialTheme.colors.background
    }
    Card(
        modifier = Modifier
            .padding(smallPadding)
            .fillMaxWidth()
            .height(dimensionResource(id = R.dimen.list_item_height))
            .clickable { onClick(index) }
            .semantics { contentDescription = "Item ${index + 1}" },
        shape = RoundedCornerShape(smallPadding),
        elevation = miniPadding
    ) {
        Surface(color = pickedColor) {
            Row(
                Modifier
                    .padding(miniPadding)
                    .fillMaxSize()
            ) {
                Image(
                    painter = rememberImagePainter(
                        data = homeModel.imageLink,
                        builder = {
                            scale(Scale.FILL)
                            placeholder(R.drawable.ic_video_camera)
                            transformations(CircleCropTransformation())
                        }
                    ),
                    contentDescription = homeModel.imageLink,
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(0.2f)
                )
                Column(
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .padding(miniPadding)
                        .fillMaxHeight()
                        .weight(0.8f)
                ) {
                    Text(
                        text = homeModel.author, style = MaterialTheme.typography.body1,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = homeModel.imageLink,
                        style = MaterialTheme.typography.subtitle1,
                        maxLines = 2,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
    }
}

@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun HomeImagePreview() {
    val previewData = MutableLiveData<Resource<List<HomeUiModel>>>()
    previewData.value = Resource.Success(
        listOf(
            HomeUiModel(
                "Charles",
                "https://downloadthis.com"
            ),
            HomeUiModel(
                "Joanna",
                "https://downloadthat.com"
            )
        )
    )
    MaterialTheme {
        HomeList(previewData, MutableLiveData(0), {}, {})
    }
}
