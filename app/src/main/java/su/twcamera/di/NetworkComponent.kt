package su.twcamera.di

import dagger.Component
import su.network.di.NetworkModule
import su.twcamera.download.DownloadFragment
import su.twcamera.home.HomeViewModelFactory
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ApiModule::class, RepositoryModule::class])
interface NetworkComponent {
    fun inject(downloadFragment: DownloadFragment)

    fun inject(homeViewModelFactory: HomeViewModelFactory)
}
