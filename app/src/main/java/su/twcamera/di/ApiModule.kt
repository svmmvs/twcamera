package su.twcamera.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import su.network.api.ImageApi
import su.network.di.NetworkModule
import su.network.di.RETROFIT_PIC_SUM
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [NetworkModule::class])
class ApiModule {
    @Provides
    @Singleton
    fun provideImageApi(@Named(RETROFIT_PIC_SUM) retrofit: Retrofit): ImageApi {
        return retrofit.create(ImageApi::class.java)
    }
}
