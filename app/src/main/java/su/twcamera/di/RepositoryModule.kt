package su.twcamera.di

import dagger.Module
import dagger.Provides
import su.network.api.ImageApi
import su.twcamera.ImageRepository
import javax.inject.Singleton

@Module(includes = [ApiModule::class])
class RepositoryModule {
    @Provides
    @Singleton
    fun provideImageRepository(imageApi: ImageApi): ImageRepository {
        return ImageRepository(imageApi)
    }
}
