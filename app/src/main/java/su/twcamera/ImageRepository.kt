package su.twcamera

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import su.network.api.ImageApi
import su.network.model.AwesomeImage

open class ImageRepository(private val imageApi: ImageApi) {
    open fun requestAwesomeImages(): Single<Response<List<AwesomeImage>>> {
        return imageApi.getImages((0..8).random()).subscribeOn(Schedulers.io())
    }
}
