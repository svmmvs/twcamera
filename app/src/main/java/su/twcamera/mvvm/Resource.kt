package su.twcamera.mvvm

sealed class Resource<out T : Any?> {

    data class Success<out T : Any?>(val data: T) : Resource<T>()
    data class Loading<out T : Any?>(val data: T? = null) : Resource<T>()
    data class Error(val exception: Throwable? = null, val errorCode: Int = -1) :
        Resource<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Loading<*> -> "Loading[data=$data]"
            is Error -> "Error[exception=$exception, errorCode=$errorCode]"
        }
    }
}
