plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdk = 31

    defaultConfig {
        minSdk = 21
        targetSdk = 31

        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
}

dependencies {
    // dependencies appearing in the api configurations will be transitively exposed
    // to consumers of the library; dependencies found in the implementation configuration
    // will not be exposed to consumers
    implementation(kotlin("stdlib-jdk8"))

    implementation("com.google.android.material:material:1.2.1")

    // firebase
    api("com.google.firebase:firebase-analytics:18.0.0")
    api("com.google.firebase:firebase-analytics-ktx:18.0.0")
    api("com.google.firebase:firebase-messaging:21.0.1")
    api("com.google.firebase:firebase-messaging-directboot:21.0.1")
    implementation("android.arch.work:work-runtime:1.0.1")
}

dependencies {
    testImplementation("org.assertj:assertj-core:3.12.2")
    testImplementation("junit:junit:4.13.1")
}
