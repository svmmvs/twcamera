package su.util.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.transition.TransitionSet
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import su.twcamera.util.R
import su.util.logs.UtilLogcat

/**
 * Bottom Navigation with permanent icon, label and no animation
 */
class FixedBottomNavigationView : BottomNavigationView {

    constructor (context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        removeTransitions()
//        disableShifting()
        disableTextSizeChange()
    }

    private fun removeTransitions() {
        try {
            val menuView = getMenuView()
            val set = getTransitionSet(menuView)
            while (set.transitionCount > 0) {
                set.getTransitionAt(0)?.let { set.removeTransition(it) }
            }
        } catch (ex: Exception) {
            UtilLogcat.printError(ex)
        }
    }

    private fun disableShifting() {
        try {
            val buttons = getMenuViewButtons(getMenuView())
            for (button in buttons) {
                try {
                    val field = button::class.java.getDeclaredField("shiftAmount")
                    field.isAccessible = true
                    field.set(button, 0)
                } catch (ex: Exception) {
                    UtilLogcat.printError(ex)
                }
            }
        } catch (ex: Exception) {
            UtilLogcat.printError(ex)
        }
    }

    private fun disableTextSizeChange() {
        for (i in 0 until childCount) {
            try {
                val item = getChildAt(i)
                if (item is BottomNavigationMenuView) {
                    for (j in 0 until item.childCount) {
                        val menuItem = item.getChildAt(j)
                        val smallLabel =
                            menuItem.findViewById<View>(com.google.android.material.R.id.smallLabel)
                        if (smallLabel is TextView) {
                            smallLabel.setPadding(0, 0, 0, 0)
                            smallLabel.textSize =
                                resources.getInteger(R.integer.bottom_navigation_text_size)
                                    .toFloat()
                        }
                        val largeLabel =
                            menuItem.findViewById<View>(com.google.android.material.R.id.largeLabel)
                        if (largeLabel is TextView) {
                            largeLabel.setPadding(0, 0, 0, 0)
                            largeLabel.textSize =
                                resources.getInteger(R.integer.bottom_navigation_text_size)
                                    .toFloat()
                        }
                    }
                }
            } catch (ex: Exception) {
                UtilLogcat.printError(ex)
            }
        }
    }

    private fun getMenuView(): BottomNavigationMenuView {
        val field = javaClass.superclass?.getDeclaredField("menuView")
        field?.isAccessible = true
        return field?.get(this) as BottomNavigationMenuView
    }

    private fun getTransitionSet(menuView: BottomNavigationMenuView): TransitionSet {
        val field = menuView::class.java.getDeclaredField("set")
        field.isAccessible = true
        return field.get(menuView) as TransitionSet
    }

    @Suppress("UNCHECKED_CAST")
    private fun getMenuViewButtons(menuView: BottomNavigationMenuView): Array<BottomNavigationItemView> {
        val field = menuView::class.java.getDeclaredField("buttons")
        field.isAccessible = true
        return field.get(menuView) as Array<BottomNavigationItemView>
    }
}
