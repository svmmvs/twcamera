package su.util.views

import android.view.View
import android.widget.TextView
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import su.twcamera.util.R

object SnackBarUtil {

    private var snackBar: Snackbar? = null

    @JvmStatic
    fun dismiss() {
        if (isShownOrQueued()) {
            snackBar!!.dismiss()
        }
    }

    @JvmStatic
    fun isShownOrQueued(): Boolean {
        return snackBar?.isShownOrQueued == true
    }

    @JvmStatic
    fun showHint(@StringRes textResource: Int, rootView: View) {
        showHint(rootView.context.getString(textResource), rootView)
    }

    @JvmStatic
    fun showHint(text: CharSequence, rootView: View) {
        show(text, rootView)
    }

    private fun buildSnackBar(
        text: CharSequence,
        rootView: View
    ): Snackbar {
        val snackBar = Snackbar.make(rootView, text, 3000)
        val textView = snackBar.view.findViewById<TextView>(R.id.snackbar_text)
        textView?.maxLines = 10
        return snackBar
    }

    private fun show(
        text: CharSequence,
        rootView: View
    ) {
        snackBar = buildSnackBar(text, rootView)
        snackBar!!.show()
    }
}
