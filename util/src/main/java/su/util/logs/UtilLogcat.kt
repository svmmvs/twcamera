package su.util.logs

import android.util.Log

const val FIRE_BASE_TAG = "Firebase Log"

object UtilLogcat {

    // annotation for java
    @JvmStatic
    fun printError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    fun printMessage(tag: String, message: String, throwable: Throwable? = null) {
        Log.w(tag, message, throwable)
    }
}
